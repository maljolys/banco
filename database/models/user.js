const {Model, DataTypes}= require('sequelize');
const sequelize= require('../db');

class user_registro extends Model{}
user_registro.init({
    cedula: DataTypes.STRING,
    nombre: DataTypes.STRING,
    apellido: DataTypes.STRING,
    telefono: DataTypes.STRING
}, {
sequelize,
modelName:"registro"

});


module.exports=user_registro;