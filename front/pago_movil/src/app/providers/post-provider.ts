import { Injectable } from '@angular/core';
import {HttpHeaders, HttpResponse  } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { map, filter, switchMap } from 'rxjs/operators';




const httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin':'*',
      'Access-Control-Allow-Credentials':'true',
      'Access-Control-Allow-Headers': 'Content-Type, X-Auth-Token, Accept, Authorization, X-Request-With, Access-Control-Request-Method, Access-Control-Request-Headers',
      'Access-Control-Allow-Methods':'POST, GET, OPTIONS, PUT, TRACE, PATCH, CONNECT', 
      'content-type':'application/json',
      })
  
    };
  

    @Injectable({
        providedIn: 'root'
    })
export class PostProvider {
	server: string = "http://127.0.0.1:3306/"; // default
	// if you test in real device "http://localhost" change use the your IP	
    // server: string = "http://192.199.122.100/IONIC4_CRUD_LOGINREGIS_PHP_MYSQL/server_api/"; 

	constructor( public http: HttpClient) {

    }
    

	postData(body, file){
        let options = ({ httpOptions,withCredentials: true});

		return this.http.post(this.server + file, JSON.stringify(body), options)
        .pipe(res => res);

	}
}