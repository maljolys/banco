import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { FooterComponent } from './footer/footer.component';
import { IonicModule } from '@ionic/angular';
import { RouteReuseStrategy, RouterLink, RouterModule } from '@angular/router';



@NgModule({
  declarations: [CabeceraComponent,FooterComponent],
  imports: [
    CommonModule,IonicModule,RouterModule

  ],
  exports:[
CabeceraComponent,FooterComponent,RouterModule

  ]

  
})


export class ComponentesModule { }
