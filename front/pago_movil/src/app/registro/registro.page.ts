import { Component, OnInit } from '@angular/core';
import { PostProvider } from '../providers/post-provider';
import { NavController, ToastController }  from '@ionic/angular'; 
import {HttpHeaders, HttpResponse  } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { AlertController,LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  cedula: string = "";
  nombre: string = "";
  apellido: string = "";
  correo: string = "";
  contrasena: string = "";
  mensaje : any[];

  disabledbutton;
  
  constructor(private postPvdr: PostProvider,public navCtrl: NavController, public toast: ToastController,public alertController: AlertController,private loadingctrl: LoadingController) { }

  ngOnInit() {
  }

 ionViewDidEnter(){
   this.disabledbutton= false;
 }
  async insert(){
if(this.cedula==''){
this.presentcedula();
}else{
  if(this.nombre==''){
    this.presentnombre();


  }else{

    if(this.apellido==''){
      this.presentapellido();


    }else{
      if(this.correo==''){
        this.presentcorreo();

      }else{
        if(this.contrasena==''){
          this.presentcontra();

        }else{

this.disabledbutton=true;
const loading = await this.loadingctrl.create({message:'Espere un momento',});
await loading.present();

let body = {
  cedula: this.cedula,
  nombre: this.nombre,
  apellido: this.apellido,
  correo: this.correo,
  contrasena: this.contrasena,

  aksi: 'register'
};

this.postPvdr.postData(body, 'registrar').subscribe((res:any)=>{
  var alertpesan = res.msg;
  if(res.success==true){
    loading.dismiss();
    this.disabledbutton=false;
   this.presentAlert(alertpesan);
  
  }else{
    loading.dismiss();
    this.disabledbutton=false;
    this.presentAlert1(alertpesan);
    
  }








});


}



      }

    }
  }

}
    

    

 

  }


  async presentAlert(a) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro de usuario',
      message: a,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.navigateRoot('/acceso');
          }
        }
     ]      });

    await alert.present();
  }


  async presentAlert1(a) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro de usuario',
      message: a,
      buttons: [
        {
          text: 'Ok',
       
        }
     ]
    });

    await alert.present();
  }



  async presentcedula() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro de usuario',
      message: 'Disculpe la Cedula no debe estar vacia',
      buttons: [
        {
          text: 'Ok'
          
        }
     ]
    });

    await alert.present();
  }


  async presentnombre() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro de usuario',
      message: 'Disculpe el nombre no debe estar vacio',
      buttons: [
        {
          text: 'Ok'
          
        }
     ]
    });

    await alert.present();
  }
  async presentcorreo() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro de usuario',
      message: 'Disculpe el correo no debe estar vacio',
      buttons: [
        {
          text: 'Ok'
          
        }
     ]
    });

    await alert.present();
  }


  async presentapellido() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro de usuario',
      message: 'Disculpe el apellido no debe estar vacio',
      buttons: [
        {
          text: 'Ok'
          
        }
     ]
    });

    await alert.present();
  }


  async presentcontra() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Registro de usuario',
      message: 'Disculpe la contraseña no debe estar vacia',
      buttons: [
        {
          text: 'Ok'
          
        }
     ]
    });

    await alert.present();
  }


}
